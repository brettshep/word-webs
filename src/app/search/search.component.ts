import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { RandomService } from "../services/random.service";

@Component({
  selector: "app-search",
  template: `
    <main>
      <h1>Start off with a word.</h1>
      <div class="circleCont">
        <div class="circle">
          <form
            autocomplete="off"
            #searchForm="ngForm"
            (ngSubmit)="submit(searchForm)"
          >
            <div
              class="inputCont"
              [class.valid]="searchForm.valid && searchForm.dirty"
            >
              <input
                name="search"
                [(ngModel)]="searchVal"
                required
                type="text"
                placeholder="Search..."
              />
              <button>
                <i class="fas fa-search"></i>
              </button>
            </div>
            <span class="or">OR</span>
            <div type="text" class="random" (click)="getRandom()">
              <i class="fas fa-random"></i>
            </div>
          </form>
        </div>
      </div>
    </main>
  `,
  styleUrls: ["./search.component.sass"]
})
export class SearchComponent implements OnInit {
  constructor(private router: Router, private random: RandomService) {}
  searchVal: string;
  ngOnInit() {}

  submit(form: NgForm) {
    if (form.valid) {
      this.router.navigateByUrl("/webs", {
        state: { search: form.value.search.trim() }
      });
    }
  }

  getRandom() {
    const term = this.random.randomWord();
    this.router.navigateByUrl("/webs", {
      state: { search: term }
    });
  }
}
