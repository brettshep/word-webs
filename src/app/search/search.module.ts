import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RouterModule, Routes } from "@angular/router";
import { SearchComponent } from "./search.component";
import { FormsModule } from "@angular/forms";

export const ROUTES: Routes = [
  {
    path: "",
    component: SearchComponent
  },
  { path: "**", redirectTo: "/search" }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), FormsModule],
  declarations: [SearchComponent]
})
export class SearchModule {}
