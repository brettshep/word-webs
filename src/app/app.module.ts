import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Store } from "./store";

// Firebase
import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";

// analytics
import {
  AngularFireAnalyticsModule,
  ScreenTrackingService
} from "@angular/fire/analytics";

const config = {
  apiKey: "AIzaSyCkhS_WgdiuDrFKzj1sDq4ZkxfgbBFntlU",
  authDomain: "word-webs.firebaseapp.com",
  databaseURL: "https://word-webs.firebaseio.com",
  projectId: "word-webs",
  storageBucket: "word-webs.appspot.com",
  messagingSenderId: "688537835480",
  appId: "1:688537835480:web:53ea067ff4571d2ac044ad",
  measurementId: "G-0S0XCHMY3H"
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(config),
    AngularFireAnalyticsModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    })
  ],
  providers: [Store, ScreenTrackingService],
  bootstrap: [AppComponent]
})
export class AppModule {}
