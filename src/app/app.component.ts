import { Component } from "@angular/core";
import {
  RouterOutlet,
  Router,
  NavigationStart,
  NavigationEnd,
  NavigationCancel
} from "@angular/router";
import { RouteAnimations } from "./animations";

@Component({
  selector: "app-root",
  template: `
    <div *ngIf="loading" class="spinner"></div>
    <div class="page" [@routeAnimation]="getDepth(myOutlet)">
      <router-outlet #myOutlet="outlet"></router-outlet>
    </div>
  `,
  styleUrls: ["./app.component.sass"],
  animations: RouteAnimations
})
export class AppComponent {
  constructor(private router: Router) {}

  loading = false;

  ngOnInit() {
    this.router.events.subscribe(e => {
      if (e instanceof NavigationStart) this.loading = true;
      if (e instanceof NavigationEnd) this.loading = false;
      if (e instanceof NavigationCancel) this.loading = false;
    });
  }

  //-------ROUTER ANIMS------
  getDepth(outlet: RouterOutlet) {
    let name = outlet.activatedRouteData["name"];
    if (name) return name;
    else return -1;
  }
}
