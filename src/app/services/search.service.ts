import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import * as pluralize from "pluralize";

@Injectable({
  providedIn: "root"
})
export class SearchService {
  maxSearchResults = 6;

  constructor(private db: AngularFireDatabase) {}

  searchTerm(term: string): Promise<string[]> {
    //decapitize
    const filteredTerm = pluralize.singular(this.decapitilize(term)).trim();

    return new Promise((res, rej) => {
      this.db
        .object(`${filteredTerm}`)
        .query.once("value")
        .then(data => {
          const words = data.val();
          if (words && words.length) {
            // capitilize, shuffle, and limit word results
            const filteredData = this.filterAPIData(words);
            res(filteredData);
          } else {
            res([]);
          }
        })
        .catch(e => {
          rej(e);
        });
    });
  }

  filterAPIData(data): string[] {
    // capitilize and sigularize each word
    const capWords = data.map(word => {
      return this.capitilize(word);
    });

    //shuffle array
    this.shuffle(capWords);

    //clip max
    capWords.length = Math.min(capWords.length, this.maxSearchResults);

    return capWords;
  }

  // #region ---Utilites
  capitilize(word: string) {
    return word
      .split(" ")
      .map(w => w.substring(0, 1).toUpperCase() + w.substring(1))
      .join(" ");
  }

  decapitilize(word: string) {
    return word
      .split(" ")
      .map(w => w.substring(0, 1).toLowerCase() + w.substring(1))
      .join(" ");
  }

  shuffle(a: any[]): any[] {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }
  //#endregion
}
