import * as Pixi from "pixi.js";
import { InteractionManager } from "./interactionManager";
import { Point, DeleteState } from "../../../../interfaces";
import { Web } from "../pixi/web";
import { Subscription } from "../emitter";
import { EventManager } from "./event-manager";
import { ColorManager } from "./colorManager";
import { Nodule } from "../pixi/node";
import { SavedManager } from "./savedManager";

export class WebManager {
  interactionManager: InteractionManager;
  scaleDown: number = 1;
  scale: number = 1;
  //stages
  dragStage: Pixi.Container;
  lineStage: Pixi.Container;
  nodeStage: Pixi.Container;
  //textures
  bgTileTex: Pixi.Texture;
  circleFillTex: Pixi.Texture;
  circleOutlineTex: Pixi.Texture;
  //sprites
  bgTileSprite: Pixi.TilingSprite;
  deleteBtnArea: Pixi.Sprite;
  deleteAreaSettings = { size: 50, yOffset: 50 };
  //drag settings
  offset: Point = { x: 0, y: 0 };
  clickStart: Point;
  //web
  web: Web;
  //subs
  subscriptions: Subscription[] = [];

  constructor(
    private app: Pixi.Application,
    private eventManager: EventManager,
    private colorManager: ColorManager,
    private savedManager: SavedManager
  ) {
    //assign interaction manager
    this.interactionManager = new InteractionManager(
      app.renderer.plugins.interaction
    );

    //calculate scale based on screen size
    const winWidth = window.innerWidth;
    if (winWidth < 1000) this.scaleDown = 0.9;
    if (winWidth < 700) this.scaleDown = 0.8;
    if (winWidth < 500) this.scaleDown = 0.6;
    if (winWidth < 400) this.scaleDown = 0.5;
    this.scale = 1 / this.scaleDown;

    //1.) create Pixi textures
    this.createTextures();

    //2.) create and stage background dot sprite
    this.createBGSprite();

    //3.) create stages for app (i.e. drag, line, node)
    this.createStages();

    //4.) create delete button sprite area and it's interactions
    this.createDeleteArea();

    //5.) create drag interactions
    this.createDragInteraction();

    //6.) create web
    this.createWeb();

    //7.) create web
    this.subscibeToEvents();

    //8.) add ticker update
    this.app.ticker.add((delta: number) => {
      this.update(delta);

      // update fps
      // this.averageFPS += this.app.ticker.FPS;
      // this.count++;

      // if (this.app.ticker.FPS < this.minFPS) this.minFPS = this.app.ticker.FPS;

      // if (this.app.ticker.FPS > this.maxFPS) this.maxFPS = this.app.ticker.FPS;
    });

    // TESTING FPS
    // setInterval(() => {
    //   console.log(
    //     "Min: ",
    //     this.minFPS,
    //     "Max: ",
    //     this.maxFPS,
    //     "Average: ",
    //     this.averageFPS / this.count
    //   );

    //   this.count = 0;
    //   this.averageFPS = 0;
    //   this.minFPS = Infinity;
    //   this.maxFPS = 0;
    // }, 1000);
  }
  // minFPS = Infinity;
  // maxFPS = 0;
  // averageFPS = 0;
  // count = 0;

  //#region ---Initialization Functions
  createTextures() {
    //create textures
    //--background dots
    this.bgTileTex = Pixi.Texture.from("assets/pixi/bg.png");

    //--circle Fill
    this.circleFillTex = Pixi.Texture.from("assets/pixi/circle.png");

    //--circle Outline
    this.circleOutlineTex = Pixi.Texture.from("assets/pixi/circleRing.png");
  }

  createBGSprite() {
    // create sprite
    this.bgTileSprite = new Pixi.TilingSprite(
      this.bgTileTex,
      this.app.screen.width,
      this.app.screen.height
    );

    //add to main app stage
    this.app.stage.addChild(this.bgTileSprite);
  }

  createDeleteArea() {
    //create sprite
    this.deleteBtnArea = new Pixi.Sprite();

    // create hitarea
    this.deleteBtnArea.hitArea = new Pixi.Rectangle(
      0,
      0,
      this.deleteAreaSettings.size,
      this.deleteAreaSettings.size
    );

    // set position
    this.deleteBtnArea.position.set(
      this.app.screen.width / 2 - this.deleteAreaSettings.size / 2,
      this.app.screen.height -
        this.deleteAreaSettings.yOffset -
        this.deleteAreaSettings.size / 2
    );

    //set interactive
    this.deleteBtnArea.interactive = true;
    this.deleteBtnArea.buttonMode = true;

    //stage to main app
    this.app.stage.addChild(this.deleteBtnArea);

    //Create it's interactions
    //--Pointer Up
    this.deleteBtnArea.on("pointerup", e => {
      //shrink delete event
      this.eventManager.deleteBtnState.emit(DeleteState.shrink);

      //delete selected node
      this.web.destroySelectedNode();
    });

    //--Pointer Over
    this.deleteBtnArea.on("pointerover", e => {
      //if hovering with search node
      if (this.web.selectedNode && this.web.selectedNode.searchNode) {
        this.eventManager.deleteBtnState.emit(DeleteState.hover);
      }
    });

    //--Pointer Out
    this.deleteBtnArea.on("pointerout", e => {
      //if hovering with search node
      this.eventManager.deleteBtnState.emit(DeleteState.hoverExit);
    });
  }

  createStages() {
    //create stages
    //--drag
    this.dragStage = new Pixi.Container();
    this.dragStage.transform.scale.set(this.scaleDown);

    //--lines
    this.lineStage = new Pixi.Container();

    //--node
    this.nodeStage = new Pixi.Container();

    //add stages to app
    this.app.stage.addChild(this.dragStage);
    this.dragStage.addChild(this.lineStage);
    this.dragStage.addChild(this.nodeStage);
  }

  createDragInteraction() {
    //Pointer Down
    this.interactionManager.pointerDownEvent.subscribe(e => {
      if (!e.currentTarget) {
        this.clickStart = {
          x: e.data.global.x - this.offset.x,
          y: e.data.global.y - this.offset.y
        };
      }
    });

    //Pointer Move
    this.interactionManager.pointerMoveEvent.subscribe(e => {
      if (this.clickStart) {
        this.offset = {
          x: e.data.global.x - this.clickStart.x,
          y: e.data.global.y - this.clickStart.y
        };

        //update drag stage
        this.dragStage.position.set(this.offset.x, this.offset.y);

        //update background tile postions
        this.bgTileSprite.tilePosition.x = this.offset.x;
        this.bgTileSprite.tilePosition.y = this.offset.y;
      }
    });

    //Pointer Up
    this.interactionManager.pointerUpEvent.subscribe(e => {
      this.clickStart = null;
    });
  }

  createWeb() {
    //Instantiate web
    this.web = new Web(
      this.interactionManager,
      this.scale,
      this.circleFillTex,
      this.circleOutlineTex,
      this.eventManager,
      this.colorManager
    );
  }

  subscibeToEvents() {
    //Subscribe to events
    //--Delete Event
    const deleteStateEvent = this.eventManager.deleteBtnState.subscribe(
      state => {
        switch (state) {
          case DeleteState.grow:
            this.deleteBtnArea.scale.set(1);
            break;
          case DeleteState.shrink:
            // this.deleteBtnArea.scale.set(0);
            break;
          default:
            break;
        }
      }
    );

    // New Search Node
    const newSearchNodeEvent = this.eventManager.newSearchNode.subscribe(e => {
      this.newSearchNode(e.searchTerm, e.data);
    });

    // Append Search Node
    const appendSearchNodeEvent = this.eventManager.appendSearchNode.subscribe(
      e => {
        this.appendSearchNode(e.data, e.parent);
      }
    );

    // Saved Word Event
    const savedEvent = this.eventManager.savedWord.subscribe(e => {
      this.toggleSaved(true, e.word);
    });

    // Unsaved Word Event
    const unsavedEvent = this.eventManager.unsaveWord.subscribe(word => {
      this.toggleSaved(false, word);
    });

    //push to subscriptions array
    this.subscriptions = [
      ...this.subscriptions,
      deleteStateEvent,
      newSearchNodeEvent,
      appendSearchNodeEvent,
      savedEvent,
      unsavedEvent
    ];
  }

  //#endregion

  // #region ---Add Nodes to Web
  newSearchNode(searchTerm: string, data: string[]) {
    //create color
    const color = this.colorManager.getNewColor();

    const parentText = this.capitilize(searchTerm);
    //create base parent node
    const parent = new Nodule({
      text: parentText,
      nodeStage: this.nodeStage,
      lineStage: this.lineStage,
      color,
      radius: Nodule.SN_RADIUS,
      fontSize: Nodule.SN_Font_Size,
      web: this.web,
      scale: this.scale
    });

    //make search node
    parent.makeSearchNode(color);

    //check if saved
    if (this.savedManager.words.includes(parentText)) {
      parent.setSaved();
    }

    //move parent to center of screen
    parent.position.x =
      (this.app.screen.width * this.scale) / 2 - this.offset.x * this.scale;
    parent.position.y =
      (this.app.screen.height * this.scale) / 2 - this.offset.y * this.scale;

    //create children
    const children = this.createNodeChildren(data, color);

    //add nodes to web
    this.web.dynamicallyAddNodes(children, parent);
  }

  appendSearchNode(data: string[], parent: Nodule) {
    //create children
    const children = this.createNodeChildren(data, parent.color);

    //add to web
    this.web.dynamicallyAddNodes(children, parent);
  }

  createNodeChildren(data: string[], color: number): Nodule[] {
    let nodeArr = [];
    const wordList = this.savedManager.words;
    for (let i = 0; i < data.length; i++) {
      // let fontSize = 18;
      // let radius = Math.max(data[i].length * fontSize * 0.4, 30);
      // if (data[i].length > 5) {
      //   radius *= 0.75;
      //   fontSize *= 0.75;
      // }
      const text = data[i];

      // get font size and radius
      const { radius, fontSize } = this.getNodeSize(text);

      // create node
      const node = new Nodule({
        text,
        nodeStage: this.nodeStage,
        lineStage: this.lineStage,
        color,
        radius,
        fontSize,
        web: this.web,
        scale: this.scale
      });

      //check if saved
      if (wordList.includes(text)) {
        node.setSaved();
      }

      nodeArr.push(node);
    }
    return nodeArr;
  }

  getNodeSize(text: string) {
    const pixRatio = window.devicePixelRatio;
    let fontSize = Nodule.Font_Size;
    let radius;

    const style = new Pixi.TextStyle({
      fontSize,
      fontFamily: "proxima-soft",
      fontWeight: "500"
    });

    const stats = Pixi.TextMetrics.measureText(text, style);
    const textWidth = stats.width * pixRatio;

    //check to see if font size needs to scale
    if (
      textWidth >=
      (Nodule.SN_RADIUS * 2 - Nodule.SN_Padding * 2) * pixRatio
    ) {
      //scale
      fontSize =
        ((Nodule.Font_Size * (Nodule.SN_RADIUS * 2 - Nodule.SN_Padding * 2)) /
          textWidth) *
        pixRatio;
      //set radius
      radius = Nodule.SN_RADIUS;
    } else {
      //set radius based on size
      radius = (textWidth / pixRatio) * 0.5 + 3;
    }

    //min radius
    radius = Math.max(radius, 30);

    return { radius, fontSize };
  }

  //#endregion

  // #region ---Handle Save State
  clearWords() {
    for (let i = 0; i < this.web.searchNodes.length; i++) {
      const parent = this.web.searchNodes[i];
      if (parent.saved) {
        parent.unsave();
      }
      //unsave children
      for (let j = 0; j < parent.connections.length; j++) {
        const child = parent.connections[j];
        if (child.searchNode) continue;

        if (child.saved) {
          child.unsave();
        }
      }
    }
  }

  toggleSaved(toggle: boolean, word: string) {
    //check if other nodes have word, if so mark it as saved
    for (let i = 0; i < this.web.searchNodes.length; i++) {
      const parent = this.web.searchNodes[i];
      if (parent.text === word) {
        toggle ? parent.setSaved() : parent.unsave();
      }
      //check children
      for (let j = 0; j < parent.connections.length; j++) {
        const child = parent.connections[j];
        if (child.searchNode) continue;
        // console.log("child: ", parent);
        if (child.text === word) {
          toggle ? child.setSaved() : child.unsave();
        }
      }
    }
  }
  //#endregion

  clearWeb() {
    this.web.clear();
  }

  update(delta: number) {
    const screenSize = {
      x: this.app.screen.width * this.scale,
      y: this.app.screen.height * this.scale
    };
    const dragOffset = {
      x: this.offset.x * this.scale,
      y: this.offset.y * this.scale
    };

    this.web.update(delta, screenSize, dragOffset);
  }

  resize() {
    const width = window.innerWidth;
    const height = window.innerHeight;

    //resize tile sprite
    this.bgTileSprite.width = width;
    this.bgTileSprite.height = height;

    //re-postion delete button area
    this.deleteBtnArea.position.set(
      width / 2 - this.deleteAreaSettings.size / 2,
      height -
        this.deleteAreaSettings.yOffset -
        this.deleteAreaSettings.size / 2
    );
  }

  destroy() {
    //destroy interaction manager
    this.interactionManager.destroy();

    //destroy web
    this.web.destroy();

    //remove deleteareas listeners
    this.deleteBtnArea.removeAllListeners();

    //unsubscribe
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  //#region ---Utilities

  capitilize(word: string) {
    return word
      .split(" ")
      .map(w => w.substring(0, 1).toUpperCase() + w.substring(1))
      .join(" ");
  }

  //#endregion
}
