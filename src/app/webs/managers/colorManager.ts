export class ColorManager {
  // background
  static bgFill = 0x14161f;

  //node colors
  static savedColor: number = 0x2b2d35;

  nodeColors = [
    0xfc4c8b,
    0xffc551,
    0x0c8ac0,
    0xff7a5d,
    0x16bbb2,
    0x37e982,
    0x0caec0,
    0xffa870,
    0x9f40ff,
    0x695aff,
    0x22b3ff,
    0xde5aff,
    0x22cca0,
    0x4785ff,
    0x8f5aff,
    0xfa4b56,
    0x00ddff,
    0xfb59e1
  ];

  //index
  colorIndex = -1;

  getNewColor(): number {
    this.colorIndex++;
    this.colorIndex %= this.nodeColors.length;
    return this.nodeColors[this.colorIndex];
  }
}
