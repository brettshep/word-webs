import * as Pixi from "pixi.js";
import { Emitter } from "../emitter";
export class InteractionManager {
  pointerDownEvent: Emitter<Pixi.interaction.InteractionEvent> = new Emitter<
    Pixi.interaction.InteractionEvent
  >();
  pointerMoveEvent: Emitter<Pixi.interaction.InteractionEvent> = new Emitter<
    Pixi.interaction.InteractionEvent
  >();
  pointerUpEvent: Emitter<Pixi.interaction.InteractionEvent> = new Emitter<
    Pixi.interaction.InteractionEvent
  >();
  pointerOutEvent: Emitter<Pixi.interaction.InteractionEvent> = new Emitter<
    Pixi.interaction.InteractionEvent
  >();

  constructor(private manager: Pixi.interaction.InteractionManager) {
    // -------------TOUCH------------
    // ----Touch Start-------
    manager.on("touchstart", (e: Pixi.interaction.InteractionEvent) => {
      const orig = e.data.originalEvent;
      orig.preventDefault();

      if ((orig as any).touches.length === 1) this.pointerDownEvent.emit(e);
    });

    //----Touch Move-------
    manager.on("touchmove", (e: Pixi.interaction.InteractionEvent) => {
      const orig = e.data.originalEvent;
      orig.preventDefault();
      if ((orig as any).touches.length === 1) this.pointerMoveEvent.emit(e);
    });

    //----Touch End-------
    manager.on("touchend", (e: Pixi.interaction.InteractionEvent) => {
      const orig = e.data.originalEvent;
      orig.preventDefault();
      this.pointerUpEvent.emit(e);
    });

    // ----------MOUSE-----------
    // ----Mouse Down-------
    manager.on("mousedown", (e: Pixi.interaction.InteractionEvent) => {
      this.pointerDownEvent.emit(e);
    });

    //----Mouse Move-------
    manager.on("mousemove", (e: Pixi.interaction.InteractionEvent) => {
      this.pointerMoveEvent.emit(e);
    });

    //----Mouse Up/Out-------
    manager.on("mouseup", (e: Pixi.interaction.InteractionEvent) => {
      this.pointerUpEvent.emit(e);
    });
    manager.on("mouseout", (e: Pixi.interaction.InteractionEvent) => {
      this.pointerUpEvent.emit(e);
    });
  }

  destroy() {
    this.manager.removeAllListeners();
  }
}
