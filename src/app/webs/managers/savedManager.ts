export class SavedManager {
  words: string[];
  notes: string;

  setWords(words: string[]) {
    this.words = [...words];
  }

  setNotes(notes: string) {
    this.notes = notes;
  }

  saveWord(word: string) {
    this.words = [...this.words, word];
  }

  unsaveWord(remove: string) {
    this.words = this.words.filter(word => word !== remove);
  }

  clearNotes() {
    this.notes = "";
  }

  clearWords() {
    this.words = [];
  }
}
