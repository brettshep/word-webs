import { Emitter } from "../emitter";
import { Nodule } from "../pixi/node";
import {
  DeleteState,
  AppendSearchNodeParams,
  NewSearchNodeParams
} from "../../../../interfaces";

export class EventManager {
  deleteBtnState: Emitter<DeleteState> = new Emitter();
  barToggle: Emitter<boolean> = new Emitter();
  barValue: Emitter<number> = new Emitter();
  savedWord: Emitter<{
    word: string;
    toggle: boolean;
    anim: boolean;
  }> = new Emitter();
  newSearchNode: Emitter<NewSearchNodeParams> = new Emitter();
  appendSearchNode: Emitter<AppendSearchNodeParams> = new Emitter();
  newSearchNodeQuery: Emitter<string> = new Emitter();
  appendSearchNodeQuery: Emitter<Nodule> = new Emitter();
  unsaveWord: Emitter<string> = new Emitter();
}
