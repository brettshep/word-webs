import {
  Component,
  OnInit,
  NgZone,
  ViewChild,
  ElementRef,
  HostListener,
  ChangeDetectorRef
} from "@angular/core";
import { Router } from "@angular/router";
import { SearchService } from "../services/search.service";
import { Application } from "pixi.js";
import { WebManager } from "./managers/web-manager";
import { EventManager } from "./managers/event-manager";
import { Subscription } from "./emitter";
import { DeleteState } from "../../../interfaces";
import { ColorManager } from "./managers/colorManager";
import * as FontObserver from "fontfaceobserver";
import { SavedManager } from "./managers/savedManager";
import { Store, SaveData } from "../store";
import { first } from "rxjs/operators";
import { RandomService } from "../services/random.service";

@Component({
  selector: "app-webs",
  template: `
    <main>
      <!------- HEADER ------->
      <app-header
        (showMenu)="showSideBar = true"
        (searchWord)="searchWordInput($event)"
        (clearWeb)="clearWeb()"
        (random)="searchRandomWord()"
        (help)="goToHome()"
        [hasContent]="(!!savedManager?.words.length || !!savedManager?.notes)"
      ></app-header>
      <!------- /HEADER ------->

      <!------- SIDEBAR ------->
      <sidebar
        *ngIf="savedManager"
        [showSideBar]="showSideBar"
        [savedWords]="savedManager.words"
        [savedNotes]="savedManager.notes"
        (exit)="showSideBar = false"
        (unsaveWord)="unsaveWord($event)"
        (newWord)="saveWords($event)"
        (saveNotes)="saveNotes($event)"
        (clearNotes)="clearNotes()"
        (clearWords)="clearWords()"
      ></sidebar>
      <!------- /SIDEBAR ------->

      <!------- CANVAS ------->
      <div class="canvasWrapper" [class.active]="pixiDone" #canvasWrapper></div>
      <!------- /CANVAS ------->

      <!------- FOOTER ------->
      <!-- Delete Button -->
      <div class="delete" #deleteBtn>
        <i class="fas fa-trash-alt"></i>
      </div>

      <!-- Progress Bar -->
      <div class="progressBarCont" #progressBarCont>
        <div class="progressBar">
          <span class="progressBarFill" #progressBarFill></span>
        </div>
      </div>

      <!-- Saved Text -->
      <div class="savedText" #savedText>SAVED</div>
      <!-- unSaved Text -->
      <div class="savedText" #unsavedText>UNSAVED</div>
      <!---- /FOOTER ---->
    </main>
  `,
  styleUrls: ["./webs.component.sass"]
})
export class WebsComponent implements OnInit {
  //progress bar
  @ViewChild("progressBarCont", { static: true })
  progressBarContRef: ElementRef;
  progressBarContElem: HTMLDivElement;

  //progress bar fill
  @ViewChild("progressBarFill", { static: true })
  progressBarFillRef: ElementRef;
  progressBarFillElem: HTMLDivElement;

  //progress bar saved text
  @ViewChild("savedText", { static: true }) savedTextRef: ElementRef;
  savedTextElem: HTMLDivElement;

  //progress bar unsaved text
  @ViewChild("unsavedText", { static: true }) unsavedTextRef: ElementRef;
  unsavedTextElem: HTMLDivElement;

  //delete button
  @ViewChild("deleteBtn", { static: true }) deleteBtnRef: ElementRef;
  deleteBtnElem: HTMLDivElement;

  //pixi
  @ViewChild("canvasWrapper", { static: true }) canvasWrapperRef: ElementRef;
  canvasWrapperElem: HTMLDivElement;
  devicePixelRatio = window.devicePixelRatio || 1;
  app: Application;
  webManager: WebManager;
  eventManager: EventManager;
  colorManager: ColorManager;
  savedManager: SavedManager;

  //events
  subscriptions: Subscription[] = [];

  //search term
  routeSearchTerm: string;

  //sidebar?
  showSideBar: boolean = false;

  //fade pixi
  pixiDone: boolean = false;

  constructor(
    private router: Router,
    private zone: NgZone,
    private searchServ: SearchService,
    private store: Store,
    private cd: ChangeDetectorRef,
    private random: RandomService
  ) {
    //grab search term from router state
    // TODO: enable route search when ready
    const state = this.router.getCurrentNavigation().extras.state;
    this.routeSearchTerm = state.search;
  }

  ngOnInit() {
    //set body overflow to hidden
    document.body.style.overflow = "hidden";

    // ensure fonts are loaded
    const fontLight = new FontObserver("proxima-soft", { weight: 500 });
    const fontBold = new FontObserver("proxima-soft", { weight: 700 });
    Promise.all([fontLight.load(), fontBold.load()]).then(() => {
      this.init();
    });
  }

  init() {
    //search for initial term

    //Assign html elements from view refs
    this.canvasWrapperElem = this.canvasWrapperRef.nativeElement;
    this.deleteBtnElem = this.deleteBtnRef.nativeElement;
    this.progressBarContElem = this.progressBarContRef.nativeElement;
    this.progressBarFillElem = this.progressBarFillRef.nativeElement;
    this.savedTextElem = this.savedTextRef.nativeElement;
    this.unsavedTextElem = this.unsavedTextRef.nativeElement;

    //1.) Create Pixi
    this.createPixi();

    //2.) create eventManager
    this.eventManager = new EventManager();

    //3.) create colormanager
    this.colorManager = new ColorManager();

    // 4.) Create Saved Manager
    this.savedManager = new SavedManager();
    this.setDataFromStore();

    //5.) create web manager
    this.webManager = new WebManager(
      this.app,
      this.eventManager,
      this.colorManager,
      this.savedManager
    );

    //6.) Subscribe to events
    this.subscribeToEvents();

    // 7.)Search for word
    // TODO: enable route search when ready
    // const term = "Pots";
    this.eventManager.newSearchNodeQuery.emit(this.routeSearchTerm);

    // this.eventManager.newSearchNodeQuery.emit(this.routeSearchTerm);
  }

  createPixi() {
    //Pixi Application Options
    const options = {
      antialias: true,
      resolution: this.devicePixelRatio,
      autoDensity: true,
      resizeTo: this.canvasWrapperElem,
      backgroundColor: ColorManager.bgFill
    };

    //Create Pixi Application outside of ngZone
    this.zone.runOutsideAngular(() => {
      this.app = new Application(options);
    });

    //Attach generated Pixi canvas to wrapper
    this.canvasWrapperElem.appendChild(this.app.view);
    this.app.view.oncontextmenu = e => {
      return false;
    };
  }

  subscribeToEvents() {
    // Delete State
    const deleteStateEvent = this.eventManager.deleteBtnState.subscribe(
      state => {
        switch (state) {
          case DeleteState.grow:
            this.deleteBtnElem.classList.toggle("active", true);
            break;

          case DeleteState.shrink:
            this.deleteBtnElem.classList.toggle("active", false);
            this.deleteBtnElem.classList.toggle("hover", false);
            break;

          case DeleteState.hover:
            this.deleteBtnElem.classList.toggle("hover", true);
            break;

          case DeleteState.hoverExit:
            this.deleteBtnElem.classList.toggle("hover", false);
            break;

          default:
            break;
        }
      }
    );

    // Progress Bar Toggle
    const barToggleEvent = this.eventManager.barToggle.subscribe(toggle => {
      //hide or show bar
      this.progressBarContElem.classList.toggle("active", toggle);
      if (!toggle) {
        this.progressBarFillElem.style.transform = `scaleX(0)`;
      }
    });

    // Progress Bar Value
    const barValueEvent = this.eventManager.barValue.subscribe(val => {
      //update bar val
      this.progressBarFillElem.style.transform = `scaleX(${val})`;
    });

    // New Search Node
    const newSearchNodeQueryEvent = this.eventManager.newSearchNodeQuery.subscribe(
      async searchTerm => {
        try {
          //get info from db
          const data = await this.searchServ.searchTerm(searchTerm);

          //emit new node event
          this.eventManager.newSearchNode.emit({ searchTerm, data });

          //pixi done
          this.pixiDone = true;
        } catch (error) {
          // on error, fail lol
          alert("Sorry, the servers are having issues!");
          console.error(error);
        }
      }
    );

    // Append Search Node
    const appendSearchNodeQueryEvent = this.eventManager.appendSearchNodeQuery.subscribe(
      async parent => {
        //set color
        let color;
        if (parent.futureColor) {
          color = parent.futureColor;
        } else {
          color = this.colorManager.getNewColor();
        }

        //make parent search node
        parent.makeSearchNode(color);

        try {
          //get term
          const term = parent.text;

          //get info from db
          const data = await this.searchServ.searchTerm(term);

          //emit append node event
          this.eventManager.appendSearchNode.emit({
            data,
            parent
          });
        } catch (error) {
          // on error, fail lol
          alert("Sorry, the servers are having issues!");
          console.error(error);
        }
      }
    );

    // Saved Word Event
    const savedEvent = this.eventManager.savedWord.subscribe(e => {
      if (e.toggle) {
        //add to save manager
        this.savedManager.saveWord(e.word);
        if (e.anim) {
          //toggle save text anim
          this.savedTextElem.classList.add("active");
          setTimeout(() => {
            this.savedTextElem.classList.remove("active");
          }, 1000);
        }
      } else {
        //remove from save manager
        this.savedManager.unsaveWord(e.word);

        if (e.anim) {
          //toggle unsave text anim
          this.unsavedTextElem.classList.add("active");
          setTimeout(() => {
            this.unsavedTextElem.classList.remove("active");
          }, 1000);
        }
      }

      // detect changes
      this.cd.detectChanges();
    });

    //push to subscriptions array
    this.subscriptions = [
      ...this.subscriptions,
      deleteStateEvent,
      barToggleEvent,
      barValueEvent,
      savedEvent,
      newSearchNodeQueryEvent,
      appendSearchNodeQueryEvent
    ];
  }

  searchWordInput(word: string) {
    this.eventManager.newSearchNodeQuery.emit(word);
  }

  searchRandomWord() {
    const term = this.random.randomWord();
    this.eventManager.newSearchNodeQuery.emit(term);
  }

  clearWeb() {
    this.webManager.clearWeb();
  }

  goToHome() {
    this.router.navigateByUrl("/home");
  }

  //#region ---Data Handling

  // Words
  unsaveWord(word: string) {
    // remove from manager
    this.savedManager.unsaveWord(word);
    //emit event
    this.eventManager.unsaveWord.emit(word);
    // update changes
    this.cd.detectChanges();
  }

  saveWords(word: string) {
    this.eventManager.savedWord.emit({ word, toggle: true, anim: false });
  }

  clearWords() {
    this.savedManager.clearWords();
    this.webManager.clearWords();
  }

  // Notes
  saveNotes(notes: string) {
    this.savedManager.notes = notes;
  }

  clearNotes() {
    this.savedManager.clearNotes();
  }

  // Store
  setDataFromStore() {
    this.store
      .select<SaveData>("savedData")
      .pipe(first())
      .subscribe(data => {
        this.savedManager.setWords(data.words);
        this.savedManager.setNotes(data.notes);
      });
  }

  saveStore() {
    this.store.set("savedData", {
      words: this.savedManager.words,
      notes: this.savedManager.notes
    });
  }

  //#endregion

  // On Destroy
  ngOnDestroy() {
    // save store
    this.saveStore();

    //set body overflow to hidden
    document.body.style.overflow = "auto";

    //unsubscribe
    this.subscriptions.forEach(sub => sub.unsubscribe());

    //remove listeners
    this.webManager.destroy();

    //destroy pixi
    this.app.destroy(true, {
      children: true,
      texture: true,
      baseTexture: true
    });
  }

  // Window resize
  @HostListener("window:resize")
  windowResize() {
    this.webManager.resize();
  }
}
