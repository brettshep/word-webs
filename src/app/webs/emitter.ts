export class Emitter<T> {
  events: Function[] = [];

  subscribe(fn: (data: T) => any): Subscription {
    this.events.push(fn);

    return {
      unsubscribe: () => {
        this.events = this.events.filter(eventFn => fn !== eventFn);
      }
    };
  }

  emit(data?: T) {
    this.events.forEach(fn => {
      fn.call(null, data);
    });
  }
}

export interface Subscription {
  unsubscribe: Function;
}

// Subscribe(eventName: string, fn: Function): Subscription {
//     if (!this.events[eventName]) {
//       this.events[eventName] = [];
//     }

//     this.events[eventName].push(fn);

//     return {
//       Unsubscribe: () => {
//         this.events[eventName] = this.events[eventName].filter(
//           eventFn => fn !== eventFn
//         );
//       }
//     };
//   }

//   Emit(eventName: string, data: any) {
//     const event = this.events[eventName];
//     if (event) {
//       event.forEach(fn => {
//         fn.call(null, data);
//       });
//     }
//   }
