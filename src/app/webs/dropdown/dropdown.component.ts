import { DropDownOptions } from "../../../../interfaces";
import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  Input
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "dropdown",
  template: `
    <div class="dropDown" #dropDown>
      <h3 *ngIf="options.random" (click)="randomEmit()">
        Random <i class="fas fa-random"></i>
      </h3>
      <h3 *ngIf="options.copy" (click)="copyEmit()">
        Copy <i class="fas fa-copy"></i>
      </h3>
      <h3 *ngIf="options.clear" (click)="clearEmit()">
        Clear <i class="fas fa-trash-alt"></i>
      </h3>
      <h3 *ngIf="options.help" (click)="helpEmit()">
        Help <i class="fas fa-question"></i>
      </h3>
      <a href="https://www.buymeacoffee.com/ngCiUbM" target="_blank"
        ><h3 *ngIf="options.donate" (click)="close.emit()">
          Donate <i class="fas fa-dollar-sign"></i></h3
      ></a>
    </div>
  `,
  styleUrls: ["./dropdown.component.sass"]
})
export class DropdownComponent {
  @ViewChild("dropDown", { static: true }) dropDownRef: ElementRef;

  @Input() options: DropDownOptions;
  @Output() close = new EventEmitter();
  @Output() copy = new EventEmitter();
  @Output() clear = new EventEmitter();
  @Output() random = new EventEmitter();
  @Output() help = new EventEmitter();

  startListening = false;

  helpEmit() {
    this.help.emit();
    this.close.emit();
  }

  copyEmit() {
    this.copy.emit();
    this.close.emit();
  }

  clearEmit() {
    this.clear.emit();
    this.close.emit();
  }

  randomEmit() {
    this.random.emit();
    this.close.emit();
  }

  onClick(e) {
    const elem = this.dropDownRef.nativeElement;
    if (!elem.contains(e.target) && this.startListening) {
      this.close.emit();
    }
  }

  ngAfterViewInit() {
    document.addEventListener("touchstart", this.onClick.bind(this));
    document.addEventListener("click", this.onClick.bind(this));
    setTimeout(() => {
      this.startListening = true;
    }, 100);
  }

  ngOnDestroy() {
    document.removeEventListener("click", this.onClick);
    document.removeEventListener("touchstart", this.onClick);
  }
}
