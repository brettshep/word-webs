import {
  Component,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  Input
} from "@angular/core";

@Component({
  selector: "section-notes",
  template: `
    <div class="cont">
      <textarea
        #textArea
        (input)="onInput()"
        placeholder="Add some notes here..."
      ></textarea>
    </div>
    <!-- <div class="textArea" contenteditable="true" (input)="onInput()"></div> -->
  `,
  styleUrls: ["./notes.component.sass"]
})
export class NotesComponent {
  @ViewChild("textArea", { static: true }) textAreaRef: ElementRef;
  textAreaElem: HTMLTextAreaElement;

  @Output() saveNotes = new EventEmitter<string>();

  @Input() set savedNotes(val: string) {
    this.textAreaElem = this.textAreaRef.nativeElement;
    this.textAreaElem.value = val;
  }

  onInput() {
    this.saveNotes.emit(this.textAreaElem.value);
  }
}
