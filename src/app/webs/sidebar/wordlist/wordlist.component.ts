import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "section-wordlist",
  template: `
    <div class="container">
      <div class="word" *ngFor="let word of savedWords.slice().reverse()">
        <h3>{{ word }}</h3>
        <i class="fas fa-times exit" (click)="deleteWord.emit(word)"></i>
      </div>
    </div>
  `,
  styleUrls: ["./wordlist.component.sass"]
})
export class WordlistComponent {
  @Input() savedWords: string[];

  @Output() deleteWord = new EventEmitter<string>();
}
