import { NgForm } from "@angular/forms";
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "sidebar",
  template: `
    <aside [class.active]="showSideBar">
      <!-- TOP SECTION -->
      <div class="top">
        <!-- Section buttons, and exit -->
        <div class="nav">
          <i class="fas fa-times exit" (click)="exit.emit()"></i>
          <button
            [class.active]="selectedCat === 'saved'"
            (click)="selectedCat = 'saved'"
          >
            <i class="fas fa-bookmark"></i>
          </button>
          <button
            [class.active]="selectedCat === 'notes'"
            (click)="selectedCat = 'notes'"
          >
            <i class="fas fa-align-left"></i>
          </button>
        </div>

        <!-- Section Title -->
        <h2 class="sectionTitle">
          {{ selectedCat === "saved" ? "Saved Words" : "Notes" }}
          <i
            class="fas fa-ellipsis-h"
            (click)="dropDownToggle = !dropDownToggle"
          ></i>
          <!-- DROPDOWN -->
          <dropdown
            class="dropdown"
            *ngIf="dropDownToggle"
            [options]="{ copy: true, clear: true }"
            (close)="dropDownToggle = false"
            (copy)="copy()"
            (clear)="clear()"
          ></dropdown>
        </h2>
      </div>

      <!-- CONTENT SECTION -->
      <div class="center">
        <div class="topGradient"></div>
        <div class="scroll" [class.active]="selectedCat === 'saved'">
          <section-wordlist
            *ngIf="selectedCat === 'saved'; else notes"
            [savedWords]="savedWords"
            (deleteWord)="unsaveWord.emit($event)"
          ></section-wordlist>

          <ng-template #notes>
            <section-notes
              (saveNotes)="saveNotes.emit($event)"
              [savedNotes]="savedNotes"
            ></section-notes>
          </ng-template>
        </div>
        <div class="bottomGradient"></div>
      </div>

      <!-- BOTTOM SECTION -->

      <form
        *ngIf="selectedCat === 'saved'"
        class="bottom"
        autocomplete="off"
        #addWordForm="ngForm"
        (ngSubmit)="newWordEmit(addWordForm)"
      >
        <input
          name="addWord"
          [(ngModel)]="addWord"
          required
          type="text"
          placeholder="Save Word..."
        />
        <button [class.valid]="addWordForm.valid && addWordForm.dirty">
          <i class="fas fa-plus"></i>
        </button>
      </form>
    </aside>
  `,
  styleUrls: ["./sidebar.component.sass"]
})
export class SidebarComponent implements OnInit {
  @Input() showSideBar: boolean;
  selectedCat: "notes" | "saved" = "saved";

  @Input() savedWords: string[];

  @Input() savedNotes: string;

  @Output() exit = new EventEmitter<any>();

  @Output() unsaveWord = new EventEmitter<string>();

  @Output() newWord = new EventEmitter<string>();

  @Output() saveNotes = new EventEmitter<string>();

  @Output() clearNotes = new EventEmitter<string>();

  @Output() clearWords = new EventEmitter<string>();

  ngOnInit() {}

  addWord: string;
  dropDownToggle: boolean;

  newWordEmit(form: NgForm) {
    if (form.valid) {
      const word = this.capitilize(form.value.addWord).trim();
      this.newWord.emit(word);
      //reset form
      form.setValue({ addWord: "" });
    }
  }

  copy() {
    if (this.selectedCat === "saved") {
      const wordString = this.savedWords
        .slice()
        .reverse()
        .join("\n");
      this.copyText(wordString);
    } else {
      this.copyText(this.savedNotes);
    }
  }

  clear() {
    if (this.selectedCat === "saved") {
      this.clearWords.emit();
    } else {
      this.clearNotes.emit();
    }
  }

  capitilize(word: string) {
    return word
      .split(" ")
      .map(w => w.substring(0, 1).toUpperCase() + w.substring(1))
      .join(" ");
  }

  copyText(text: string) {
    //create elem and copy
    let textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.style.position = "fixed";
    textArea.style.background = "transparent";
    textArea.style.top = "0";
    textArea.style.left = "0";

    if (this.isIOS) {
      textArea.contentEditable = "true";
      textArea.readOnly = true;

      // create a selectable range
      var range = document.createRange();
      range.selectNodeContents(textArea);

      // select the range
      var selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
      textArea.setSelectionRange(0, 999999);
    } else {
      textArea.focus();
      textArea.select();
    }

    document.execCommand("copy");
    document.body.removeChild(textArea);
  }

  get isIOS(): boolean {
    return !!navigator.userAgent.match(/ipad|ipod|iphone/i);
  }
}
