import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { WebsComponent } from "./webs.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { HeaderComponent } from "./header/header.component";
import { WordlistComponent } from "./sidebar/wordlist/wordlist.component";
import { NotesComponent } from "./sidebar/notes/notes.component";
import { DropdownComponent } from './dropdown/dropdown.component';

export const ROUTES: Routes = [
  {
    path: "",
    component: WebsComponent
  },
  { path: "**", redirectTo: "/webs" }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), FormsModule],
  declarations: [
    WebsComponent,
    SidebarComponent,
    HeaderComponent,
    WordlistComponent,
    NotesComponent,
    DropdownComponent
  ]
})
export class WebsModule {}
