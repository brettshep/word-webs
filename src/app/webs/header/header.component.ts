import {
  Component,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  HostListener,
  ViewChild,
  Input
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { ElementRef } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "app-header",
  template: `
    <header>
      <!-- Logo -->
      <div class="left">
        <img src="assets/logoLight.png" alt="logo" />
      </div>

      <!-- Input -->
      <div class="center">
        <div type="text" class="random" (click)="random.emit()">
          <i class="fas fa-random"></i>
        </div>
        <form
          autocomplete="off"
          #searchForm="ngForm"
          (ngSubmit)="newWordEmit(searchForm)"
        >
          <input
            #inputField
            name="searchWord"
            [(ngModel)]="searchWordText"
            required
            type="text"
            placeholder="New Word..."
          />
          <button [class.valid]="searchForm.valid && searchForm.dirty">
            <i class="fas fa-plus"></i>
          </button>
        </form>
      </div>

      <!-- Menu -->
      <div class="right">
        <a href="https://www.buymeacoffee.com/ngCiUbM" target="_blank">
          <button class="donationButton">
            <span><i class="fas fa-dollar-sign"></i></span>
            SUPPORT US
          </button></a
        >
        <i
          class="fas fa-ellipsis-h icon"
          (click)="dropDownToggle = !dropDownToggle"
        ></i>
        <i class="fas fa-bars icon menuBars" (click)="showMenu.emit()">
          <span [class.active]="hasContent"></span>
        </i>
        <!-- DROPDOWN -->
        <dropdown
          class="dropdown"
          *ngIf="dropDownToggle"
          [options]="{ clear: true, random: true, help: true, donate: true }"
          (close)="dropDownToggle = false"
          (clear)="clearWeb.emit()"
          (random)="random.emit()"
          (help)="help.emit()"
        ></dropdown>
      </div>
    </header>
  `,
  styleUrls: ["./header.component.sass"]
})
export class HeaderComponent {
  @ViewChild("inputField", { static: true }) inputRef: ElementRef;
  @Output() showMenu = new EventEmitter();
  @Output() searchWord = new EventEmitter();
  @Output() clearWeb = new EventEmitter();
  @Output() random = new EventEmitter();
  @Output() help = new EventEmitter();
  @Input() hasContent: boolean;

  dropDownToggle: boolean = false;
  searchWordText: string;

  @HostListener("window:touchstart")
  touchOutside() {}

  newWordEmit(form: NgForm) {
    if (form.valid) {
      const word = form.value.searchWord;
      this.searchWord.emit(word.trim());
      //reset form
      form.setValue({ searchWord: "" });
    }
  }

  onClick(e) {
    const elem: HTMLInputElement = this.inputRef.nativeElement;
    if (!elem.contains(e.target)) {
      elem.blur();
    }
  }

  ngAfterViewInit() {
    document.addEventListener("touchstart", this.onClick.bind(this));
    document.addEventListener("click", this.onClick.bind(this));
  }

  ngOnDestroy() {
    document.removeEventListener("click", this.onClick);
    document.removeEventListener("touchstart", this.onClick);
  }
}
