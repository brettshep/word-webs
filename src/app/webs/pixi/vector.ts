import { Point } from "../../../../interfaces";

export class Vector {
  constructor(public magnitude: number, public direction: number) {
    if (magnitude < 0) {
      // resolve negative magnitude by reversing direction
      magnitude = -magnitude;
      direction = (180.0 + direction) % 360;
    }

    // resolve negative direction
    if (direction < 0) direction = 360.0 + direction;
  }

  add(a: Vector): Vector {
    // break into x-y components
    let aX = a.magnitude * Math.cos((Math.PI / 180.0) * a.direction);
    let aY = a.magnitude * Math.sin((Math.PI / 180.0) * a.direction);

    let bX = this.magnitude * Math.cos((Math.PI / 180.0) * this.direction);
    let bY = this.magnitude * Math.sin((Math.PI / 180.0) * this.direction);

    // add x-y components
    aX += bX;
    aY += bY;

    // pythagorus' theorem to get resultant magnitude
    let magnitude = Math.sqrt(Math.pow(aX, 2) + Math.pow(aY, 2));

    // calculate direction using inverse tangent
    let direction;
    if (magnitude == 0) direction = 0;
    else direction = (180.0 / Math.PI) * Math.atan2(aY, aX);

    return new Vector(magnitude, direction);
  }

  scale(fac: number): Vector {
    return new Vector(this.magnitude * fac, this.direction);
  }

  toPoint(): Point {
    // break into x-y components
    let aX = this.magnitude * Math.cos((Math.PI / 180.0) * this.direction);
    let aY = this.magnitude * Math.sin((Math.PI / 180.0) * this.direction);

    return { x: aX, y: aY };
  }
}
