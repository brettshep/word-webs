import { Web } from "./web";
import { Vector } from "./vector";
import * as Pixi from "pixi.js";
import { Point } from "../../../../interfaces";
import { ColorManager } from "../managers/colorManager";

export interface NoduleParams {
  text: string;
  color: number;
  nodeStage: Pixi.Container;
  lineStage: Pixi.Container;
  radius: number;
  fontSize: number;
  web: Web;
  scale: number;
}

export class Nodule {
  static defaultSpringLength = 200;
  static SN_RADIUS = 60;
  static SN_Font_Size = 26;
  static Font_Size = 18;
  static SN_Padding = -5;
  springLength = Nodule.defaultSpringLength;
  web: Web;
  position: Point = { x: 0, y: 0 };
  parent: Nodule;
  connections: Nodule[] = [];
  bgFill = ColorManager.bgFill;
  color: number;
  radius: number;
  fontSize: number;
  circleStrokeWidth = 4;
  lineStrokeWidth = 4.5;
  selected: boolean;
  searchNode = false;
  velocity: Vector = new Vector(0, 0);

  startDrawLength = 0;
  wiggleOffset = { x: 0, y: 0, percent: 0 };
  scale: number = 1;

  //click settings
  maxClickDist = 50;
  mouseDownPos: Point;

  text: string;
  futureColor: number;
  interpolatedColor: number;
  saved: boolean = false;

  //PIXI STUFF
  nodeStage: Pixi.Container;
  lineStage: Pixi.Container;
  linesGraphics: Pixi.Graphics;
  textGraphic: Pixi.Text;
  circleFillSprite: Pixi.Sprite;
  circleOutlineSprite: Pixi.Sprite;

  constructor(params: NoduleParams) {
    this.nodeStage = params.nodeStage;
    this.lineStage = params.lineStage;
    this.color = params.color;
    this.text = params.text;
    this.radius = params.radius;
    this.fontSize = params.fontSize;
    this.web = params.web;
    this.scale = params.scale;

    //create graphics and add to stage
    this.createGraphics();
  }

  destroy() {
    //remove self
    this.circleOutlineSprite.removeChild(this.circleFillSprite);
    this.circleOutlineSprite.removeChild(this.textGraphic);
    this.nodeStage.removeChild(this.circleOutlineSprite);

    //destroy self
    this.circleFillSprite.destroy();
    this.circleOutlineSprite.destroy();
    this.textGraphic.destroy();

    //is search node
    if (this.searchNode) {
      //remove lines
      this.lineStage.removeChild(this.linesGraphics);
      this.linesGraphics.destroy;

      //destroy children
      for (let i = this.connections.length - 1; i >= 0; i--) {
        const child = this.connections[i];
        if (child.searchNode) {
          child.parent = null;
          continue;
        } else {
          child.destroy();
        }
      }
      //remove from diagram
      this.web.searchNodes = this.web.searchNodes.filter(node => node !== this);
    }
    //only child node so remove connection from parent
    else {
      this.parent.connections = this.parent.connections.filter(
        node => node !== this
      );

      // this.parent.fillLineGraphics();
    }
  }

  createGraphics() {
    this.circleOutlineSprite = new Pixi.Sprite(this.web.circleOutlineTex);
    this.circleOutlineSprite.anchor.set(0.5);
    this.circleOutlineSprite.scale.set(this.radius / Nodule.SN_RADIUS);
    this.circleOutlineSprite.interactive = true;
    this.circleOutlineSprite.buttonMode = true;
    this.circleOutlineSprite.interactiveChildren = false;

    this.setTint();
    //inner circle
    this.circleFillSprite = new Pixi.Sprite(this.web.circleFillTex);
    this.circleFillSprite.anchor.set(0.5);
    this.circleFillSprite.tint = this.bgFill;

    const innerScale =
      (this.radius - this.circleStrokeWidth + 0.75) / this.radius;
    this.circleFillSprite.scale.set(innerScale);
    this.circleOutlineSprite.addChild(this.circleFillSprite);

    //add events for node
    this.addNodeEvents();

    this.nodeStage.addChild(this.circleOutlineSprite);

    //add text
    this.textGraphic = new Pixi.Text(this.text);
    this.setTextStyle();

    this.textGraphic.anchor.set(0.5);
    this.textGraphic.scale.set(Nodule.SN_RADIUS / this.radius);
    //add as child to node
    this.circleOutlineSprite.addChild(this.textGraphic);
  }

  setTextStyle(sn?) {
    this.textGraphic.style = {
      fontSize: this.fontSize,
      fill: 0xffffff,
      fontFamily: "proxima-soft",
      fontWeight: sn ? 700 : 500
    };
  }

  setTint(wiggle?: boolean) {
    this.circleOutlineSprite.tint = !wiggle
      ? this.color
      : this.interpolatedColor;
  }

  setWiggleTransform(wiggle?: boolean) {
    let radius = this.radius;
    if (wiggle) {
      radius =
        this.radius +
        (Nodule.SN_RADIUS - this.radius) * this.wiggleOffset.percent;
    }
    this.circleOutlineSprite.scale.set(radius / Nodule.SN_RADIUS);
    this.textGraphic.scale.set(Nodule.SN_RADIUS / radius);
    const innerScale = (radius - this.circleStrokeWidth + 0.75) / radius;
    this.circleFillSprite.scale.set(innerScale);
  }

  setSaved() {
    this.circleFillSprite.tint = ColorManager.savedColor;
    this.saved = true;
  }

  unsave() {
    this.circleFillSprite.tint = ColorManager.bgFill;
    this.saved = false;
  }

  addNodeEvents() {
    // ---Touch Events--
    this.circleOutlineSprite.on(
      "touchstart",
      (e: Pixi.interaction.InteractionEvent) => {
        const orig = e.data.originalEvent;
        orig.preventDefault();
        if ((orig as any).touches.length === 1) this.pointerDown(e);
      }
    );
    this.circleOutlineSprite.on(
      "touchend",
      (e: Pixi.interaction.InteractionEvent) => {
        const orig = e.data.originalEvent;
        orig.preventDefault();
        this.pointerUp(e);
      }
    );

    // ---Mouse Events--
    this.circleOutlineSprite.on(
      "mousedown",
      (e: Pixi.interaction.InteractionEvent) => {
        this.pointerDown(e);
      }
    );
    this.circleOutlineSprite.on(
      "mouseup",
      (e: Pixi.interaction.InteractionEvent) => {
        this.pointerUp(e);
      }
    );
  }

  pointerDown(e: Pixi.interaction.InteractionEvent) {
    //reset click time to 0
    this.web.clickTime = 0;

    const mousePos: Point = { x: e.data.global.x, y: e.data.global.y };

    this.mouseDownPos = mousePos;

    // set as diagrams selected node
    this.web.selectedNode = this;

    //set to selected
    this.selected = true;

    this.web.nodeSelectOffset.x = mousePos.x * this.scale - this.x;
    this.web.nodeSelectOffset.y = mousePos.y * this.scale - this.y;

    //set node dragDist for wiggle if not search node
    if (!this.searchNode) {
      const parentCoords: Point = {
        x: this.parent.position.x,
        y: this.parent.position.y
      };

      const dist = Math.hypot(parentCoords.x - this.x, parentCoords.y - this.y);

      this.startDrawLength = dist;

      //set future color
      if (!this.futureColor) {
        this.futureColor = this.web.colorManager.getNewColor();
      }
    }

    // if (!this.saved) {
    this.web.saving = true;
    // }
  }

  pointerUp(e: Pixi.interaction.InteractionEvent) {
    if (!this.mouseDownPos) return;
    const mousePos: Point = { x: e.data.global.x, y: e.data.global.y };
    const distFromMouseDown = Math.hypot(
      this.mouseDownPos.x - mousePos.x,
      this.mouseDownPos.y - mousePos.y
    );
    this.mouseDownPos = null;

    //check if clicking child node
    if (!this.searchNode) {
      if (this.web.clickTime <= 30 && distFromMouseDown < 50) {
        this.web.eventManager.appendSearchNodeQuery.emit(this);
        return;
      }
    }
  }

  fillLineGraphics() {
    this.linesGraphics.clear();
    this.linesGraphics.lineStyle(this.lineStrokeWidth, this.color);

    //if parent, draw to it
    if (this.parent) {
      const from = {
        x: this.position.x,
        y: this.position.y
      };
      const to = {
        x: this.parent.position.x,
        y: this.parent.position.y
      };
      this.linesGraphics.moveTo(from.x, from.y);
      this.linesGraphics.lineTo(to.x, to.y);
    }

    //draw to connections
    for (let i = 0; i < this.connections.length; i++) {
      const child = this.connections[i];
      if (child.searchNode) continue;

      //create lines
      const from = {
        x: this.position.x,
        y: this.position.y
      };
      const to = {
        x: child.position.x,
        y: child.position.y
      };
      // see if node is selected, and draw appropriate line color
      if (!child.selected) {
        this.linesGraphics.lineStyle(this.lineStrokeWidth, this.color);
      } else {
        this.linesGraphics.lineStyle(
          this.lineStrokeWidth,
          child.interpolatedColor
        );
        //add wiggle offset
        to.x += child.wiggleOffset.x;
        to.y += child.wiggleOffset.y;
      }

      this.linesGraphics.moveTo(from.x, from.y);
      this.linesGraphics.lineTo(to.x, to.y);
    }
  }

  makeSearchNode(color: number) {
    //set bool
    this.searchNode = true;

    //update display properties
    this.radius = Nodule.SN_RADIUS;
    this.fontSize = Nodule.SN_Font_Size;
    this.color = color;

    const style = new Pixi.TextStyle({
      fontSize: Nodule.SN_Font_Size,
      fontFamily: "proxima-soft",
      fontWeight: "700"
    });

    const stats = Pixi.TextMetrics.measureText(this.text, style);
    const textWidth = stats.width * window.devicePixelRatio;

    //check to see if font size needs to change
    if (
      textWidth >=
      (Nodule.SN_RADIUS * 2 - Nodule.SN_Padding * 2) * window.devicePixelRatio
    ) {
      //scale
      this.fontSize =
        ((Nodule.SN_Font_Size *
          (Nodule.SN_RADIUS * 2 - Nodule.SN_Padding * 2)) /
          textWidth) *
        window.devicePixelRatio;
    }

    //update node graphics to sn
    this.circleOutlineSprite.tint = this.color;

    //update inner circle scale
    this.circleFillSprite.scale.set(
      (this.radius - this.circleStrokeWidth + 0.75) / this.radius
    );

    //update text style
    this.setTextStyle(true);
    this.circleOutlineSprite.scale.set(Nodule.SN_RADIUS / this.radius);
    this.textGraphic.scale.set(this.radius / Nodule.SN_RADIUS);

    //create line graphics
    this.linesGraphics = new Pixi.Graphics();
    this.fillLineGraphics();
    this.lineStage.addChild(this.linesGraphics);
  }

  addChild(child: Nodule) {
    this.connections.push(child);
  }

  interpolateColors(a: number, b: number, ratio: number): number {
    const mask1 = 0x00ff00ff;
    const mask2 = 0xff00ff00;

    const f2 = ~~(256 * ratio);
    const f1 = 256 - f2;

    return (
      ((((a & mask1) * f1 + (b & mask1) * f2) >> 8) & mask1) |
      ((((a & mask2) * f1 + (b & mask2) * f2) >> 8) & mask2)
    );
  }

  calculateWiggle() {
    const distFromParent = Math.hypot(
      this.parent.x - this.x,
      this.parent.y - this.y
    );

    this.wiggleOffset.percent = Math.max(
      this.map(
        distFromParent - this.startDrawLength,
        0,
        this.web.pullDist - this.startDrawLength,
        0,
        1
      ),
      0
    );

    const wiggleStrength = Math.max(20 * (this.wiggleOffset.percent - 0.6), 0);
    this.wiggleOffset.x = this.web.randRange(-1, 1) * wiggleStrength;
    this.wiggleOffset.y = this.web.randRange(-1, 1) * wiggleStrength;

    this.interpolatedColor = this.interpolateColors(
      this.color,
      this.futureColor,
      this.wiggleOffset.percent
    );
  }

  map(value, x1, y1, x2, y2) {
    return ((value - x1) * (y2 - x2)) / (y1 - x1) + x2;
  }

  toggleChildren(show: boolean) {
    // this.linesGraphics.visible = show;
    for (let i = 0; i < this.connections.length; i++) {
      const node = this.connections[i];
      if (node.searchNode) continue;
      node.circleOutlineSprite.visible = show;
    }
  }

  //getters and setters
  get x() {
    return this.position.x;
  }
  set x(val) {
    this.position.x = val;
  }
  get y() {
    return this.position.y;
  }
  set y(val) {
    this.position.y = val;
  }
}
