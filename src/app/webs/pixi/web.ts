import * as Pixi from "pixi.js";
import { Nodule } from "./node";
import { Vector } from "./vector";
import { Emitter, Subscription } from "../emitter";
import { InteractionManager } from "../managers/interactionManager";
import { Point, DeleteState } from "../../../../interfaces";
import { EventManager } from "../managers/event-manager";
import { ColorManager } from "../managers/colorManager";

export class Web {
  ATTRACTION_CONSTANT = 0.1; // spring constant
  REPULSION_CONSTANT = 50000; // charge constant
  DAMPING = 0.5;
  CENTER_FORCE_STRENGTH = 1;
  MAX_FORCE = 40;
  CENTER: Point = { x: 0, y: 0 };
  FORCE_EFFECT_RADIUS = 350;
  DRAWING_CULLING_RADIUS = 300;
  FORCE_CULLING_RADIUS = 300;
  pullDist = 500;

  searchNodes: Nodule[] = [];
  selectedNode: Nodule = null;
  nodeSelectOffset: Point = { x: 0, y: 0 };
  clickTime = 0;

  //savebar
  saveTime = 60;
  saveDelay = 20;
  saving = false;

  //world events
  subscriptions: Subscription[] = [];

  constructor(
    interactionManager: InteractionManager,
    private scale: number,
    public circleFillTex: Pixi.Texture,
    public circleOutlineTex: Pixi.Texture,
    public eventManager: EventManager,
    public colorManager: ColorManager
  ) {
    const pDown = interactionManager.pointerDownEvent.subscribe(e => {
      this.pointerDown(e);
    });
    const pMove = interactionManager.pointerMoveEvent.subscribe(e => {
      this.pointerMove(e);
    });
    const pUp = interactionManager.pointerUpEvent.subscribe(e => {
      this.pointerUp(e);
    });

    //lower pulldist on smaller screens
    const winWidth = window.innerWidth;
    if (winWidth < 1200) this.pullDist = 400;
    if (winWidth < 500) this.pullDist = 350;

    //push subs
    this.subscriptions.push(pDown, pMove, pUp);
  }

  destroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  clear() {
    for (let i = this.searchNodes.length - 1; i >= 0; i--) {
      const node = this.searchNodes[i];
      node.destroy();
    }
  }

  //#region ---World Pointer Events
  pointerDown(e: Pixi.interaction.InteractionEvent) {
    if (this.selectedNode && this.selectedNode.searchNode) {
      this.eventManager.deleteBtnState.emit(DeleteState.grow);
    }
  }
  pointerMove(e: Pixi.interaction.InteractionEvent) {
    const mousePos: Point = {
      x: e.data.global.x * this.scale,
      y: e.data.global.y * this.scale
    };
    //make sure node is selected
    if (this.selectedNode) {
      const newNodePos: Point = {
        x: mousePos.x - this.nodeSelectOffset.x,
        y: mousePos.y - this.nodeSelectOffset.y
      };

      //set position to mouse
      this.selectedNode.position.x = newNodePos.x;
      this.selectedNode.position.y = newNodePos.y;

      // //check if moving to disable word save
      if (this.saving) {
        const moveDist = Math.hypot(
          this.selectedNode.mouseDownPos.x * this.scale - mousePos.x,
          this.selectedNode.mouseDownPos.y * this.scale - mousePos.y
        );

        if (moveDist > 10) {
          this.saving = false;
          this.eventManager.barToggle.emit(false);
        }
      }

      //check pull if not search node
      if (!this.selectedNode.searchNode) {
        const parentCoords: Point = {
          x: this.selectedNode.parent.position.x,
          y: this.selectedNode.parent.position.y
        };

        const dist = Math.hypot(
          parentCoords.x - this.selectedNode.x,
          parentCoords.y - this.selectedNode.y
        );
        //distance has surpassed threshold
        if (dist > this.pullDist) {
          this.selectedNode.parent.connections = this.selectedNode.parent.connections.filter(
            node => node != this.selectedNode
          );
          //remove parent from node
          this.selectedNode.parent = null;
          //emit to create
          this.eventManager.appendSearchNodeQuery.emit(this.selectedNode);
          //toggle delete btn
          this.eventManager.deleteBtnState.emit(DeleteState.grow);
        }
      }
    }
  }

  pointerUp(e: Pixi.interaction.InteractionEvent) {
    //toggle saving
    if (this.saving) {
      this.saving = false;
      this.eventManager.barToggle.emit(false);
    }

    //clear delete button
    this.eventManager.deleteBtnState.emit(DeleteState.shrink);

    //clear selected node
    if (this.selectedNode) {
      if (!this.selectedNode.searchNode) {
        this.selectedNode.setTint();
        this.selectedNode.setWiggleTransform();
      }
      this.selectedNode.selected = false;
      this.selectedNode = null;
    }
  }
  //#endregion

  //#region ---Node Positioning and Adding ----

  update(delta: number, screenSize: Point, dragOffset: Point) {
    //update click time
    this.clickTime += delta;

    //update savebar
    if (this.saving && this.clickTime > this.saveDelay) {
      this.eventManager.barToggle.emit(true);
      const val = (this.clickTime - this.saveDelay) / this.saveTime;
      if (val >= 1) {
        this.saving = false;
        //unsave
        if (this.selectedNode.saved) {
          this.eventManager.savedWord.emit({
            word: this.selectedNode.text,
            toggle: false,
            anim: true
          });
          this.selectedNode.unsave();
        }
        //save
        else {
          this.selectedNode.setSaved();
          this.eventManager.savedWord.emit({
            word: this.selectedNode.text,
            toggle: true,
            anim: true
          });
        }
        this.eventManager.barToggle.emit(false);
      }
      this.eventManager.barValue.emit(this.easeInOutCubic(val));
    }

    //sets positions of nodes
    this.arrange(screenSize, dragOffset);

    //if node is selected, update graphics
    if (this.selectedNode) {
      if (!this.selectedNode.searchNode) {
        //calculate wiggle
        this.selectedNode.calculateWiggle();
        //set wiggle and tint
        this.selectedNode.setTint(true);
        this.selectedNode.setWiggleTransform(true);
        //set position
        this.selectedNode.circleOutlineSprite.position.set(
          this.selectedNode.x + this.selectedNode.wiggleOffset.x,
          this.selectedNode.y + this.selectedNode.wiggleOffset.y
        );
      } else {
        this.selectedNode.circleOutlineSprite.position.set(
          this.selectedNode.x,
          this.selectedNode.y
        );
      }
    }
    //set positions of search nodes and update their lines,
    for (let i = 0; i < this.searchNodes.length; i++) {
      const sn = this.searchNodes[i];
      if (sn !== this.selectedNode)
        sn.circleOutlineSprite.position.set(sn.position.x, sn.position.y);
      sn.fillLineGraphics();
    }
  }

  arrange(screenSize: Point, dragOffset: Point) {
    //get canvas dimensions for cull
    let nodesToSearch = [];

    // 1
    for (let i = this.searchNodes.length; i--; ) {
      const sn = this.searchNodes[i];
      //cull
      if (
        sn.position.x + dragOffset.x < -this.FORCE_CULLING_RADIUS ||
        sn.position.y + dragOffset.y < -this.FORCE_CULLING_RADIUS ||
        sn.position.x + dragOffset.x >
          screenSize.x + this.FORCE_CULLING_RADIUS ||
        sn.position.y + dragOffset.y > screenSize.y + this.FORCE_CULLING_RADIUS
      ) {
        //hide children
        if (sn.circleOutlineSprite.visible) {
          sn.toggleChildren(false);
          sn.circleOutlineSprite.visible = false;
        }
        continue;
      }
      //hide children
      if (!sn.circleOutlineSprite.visible) {
        //show children
        sn.toggleChildren(true);
        sn.circleOutlineSprite.visible = true;
      }

      //reset array
      nodesToSearch.length = 0;

      //check surrounding searchnodes to see if they will affect
      // 2
      for (let j = this.searchNodes.length; j--; ) {
        const otherSn = this.searchNodes[j];
        // if (otherSn === sn) continue;

        //check if within distance
        const dist = this.calcDistance(sn.position, otherSn.position);

        if (dist < this.FORCE_EFFECT_RADIUS * 2) {
          nodesToSearch.push(otherSn);
        }
      }

      //calculate children forces
      // 8
      for (let j = sn.connections.length; j--; ) {
        const child = sn.connections[j];
        if (child.searchNode || child.selected) continue;

        //calculate current vector (magnitude,direction) of node
        const currentVector = new Vector(
          this.calcDistance({ x: 0, y: 0 }, child.position),
          this.calcDirection(this.CENTER, child.position)
        );

        //net force to be applied to node
        let netForce = new Vector(0, 0);

        //attraction forces from parent
        const attractionForce = this.calcAttractionForce(
          child,
          sn,
          child.springLength
        );
        netForce = netForce.add(attractionForce);

        // 14
        for (let k = nodesToSearch.length; k--; ) {
          const otherSn = nodesToSearch[k];

          //add repulsion from other seach node
          const snRuplusion = this.calcRepulsionForce(child, otherSn);
          netForce = netForce.add(snRuplusion);

          //add forces from its children
          // 50
          for (let m = otherSn.connections.length; m--; ) {
            const otherSnChild: Nodule = otherSn.connections[m];

            //dont apply repulsion from self Or selected
            if (otherSnChild === child || otherSnChild.selected) continue;

            const ruplusionChild = this.calcRepulsionForce(child, otherSnChild);
            netForce = netForce.add(ruplusionChild);
          }
        }
        // apply net force to node velocity
        child.velocity = child.velocity.add(netForce).scale(this.DAMPING);

        //clamp to max velocity
        if (child.velocity.magnitude > this.MAX_FORCE)
          child.velocity.magnitude = this.MAX_FORCE;

        // apply velocity to node position and set point
        child.position = currentVector.add(child.velocity).toPoint();

        //-----SET PIXI POSITION------
        child.circleOutlineSprite.position.set(
          child.position.x,
          child.position.y
        );
      }
    }
  }

  dynamicallyAddNodes(nodes: Nodule[], parent: Nodule) {
    //add parent to search nodes
    this.searchNodes.push(parent);

    nodes.forEach(node => {
      //give node random position
      const range = 100;
      node.position.x = this.randRange(-range, range) + parent.x;
      node.position.y = this.randRange(-range, range) + parent.y;
      node.parent = parent;

      //add parent connection
      parent.connections.push(node);
    });
  }

  destroySelectedNode() {
    if (this.selectedNode && this.selectedNode.searchNode) {
      this.selectedNode.destroy();
      this.selectedNode = null;
    }
  }

  //#endregion

  //#region ---Force Calculations---

  calcAttractionForce(x: Nodule, y: Nodule, springLength: number) {
    const proximity = Math.max(this.calcDistance(x.position, y.position), 1);

    // Hooke's Law: F = -kx
    const force =
      this.ATTRACTION_CONSTANT * Math.max(proximity - springLength, 0);
    // const angle = this.getBearingAngle(x.position, y.position);
    const angle = this.calcDirection(x.position, y.position);

    return new Vector(force, angle);
  }

  calcRepulsionForce(x: Nodule, y: Nodule) {
    const proximity = Math.max(this.calcDistance(x.position, y.position), 1);

    // Coulomb's Law: F = k(Qq/r^2)
    const force = -(this.REPULSION_CONSTANT / proximity ** 2);

    const angle = this.calcDirection(x.position, y.position);

    return new Vector(force, angle);
  }

  //#endregion

  //#region ----Helpers----

  calcDirection(start: Point, end: Point): number {
    return (Math.atan2(end.y - start.y, end.x - start.x) * 180) / Math.PI;
  }

  calcDistance(a: Point, b: Point) {
    return Math.hypot(a.x - b.x, a.y - b.y);
  }

  randRange(min: number, max: number) {
    return Math.random() * (max - min) + min;
  }

  private easeInOutCubic = (t: number) => {
    return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
  };
  //#endregion
}
