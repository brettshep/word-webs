import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { WebsGuard } from "./guards/webs.guard";

export enum CompName {
  home = 1,
  search,
  webs
}

const routes: Routes = [
  {
    path: "home",
    data: { name: CompName.home },
    loadChildren: () => import("./home/home.module").then(m => m.HomeModule)
  },
  {
    path: "search",
    data: { name: CompName.search },
    loadChildren: () =>
      import("./search/search.module").then(m => m.SearchModule)
  },
  {
    path: "webs",
    data: { name: CompName.webs },
    canActivate: [WebsGuard],
    loadChildren: () => import("./webs/webs.module").then(m => m.WebsModule)
  },
  { path: "**", redirectTo: "home", pathMatch: "full" }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
