import { Nodule } from "./src/app/webs/pixi/node";
export interface Point {
  x: number;
  y: number;
}

export interface NewSearchNodeParams {
  searchTerm: string;
  data: string[];
}

export interface AppendSearchNodeParams {
  data: string[];
  parent: Nodule;
}

export enum DeleteState {
  grow = 1,
  shrink,
  hover,
  hoverExit
}

export interface DropDownOptions {
  copy?: boolean;
  clear?: boolean;
  random?: boolean;
  help?: boolean;
  donate?: boolean;
}
